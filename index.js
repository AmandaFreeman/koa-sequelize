// npm install -g nodemon    
// npm install koa koa-body koa-router --save   
// npm install sequelize --save   
// npm install mysql2 --sav

const Sequelize = require('sequelize');
const bcrypt = require('bcryptjs');
const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const router = new Router();

const connection = new Sequelize('testing_db', 'root', 'password', {host: 'localhost', dialect: 'mysql'});

const Record = connection.define('record', {
    title: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {
            // len: [2, 10]

            // see validation documentation for more validation rules. 

            // null values will not be validated if allowNull: true
            
            // custom error optional
            len: {
                args: [2, 10],
                msg: "\n\nThis is a custom error message to tell you that the title should be between 2 and 10 characters long\n"
            },
            // you can define your own validation functions
            startsWithUpper: function (bodyVal) {
                var first = bodyVal.charAt(0);
                var startsWithUpper = first === first.toUpperCase();
                if (!startsWithUpper) {
                    throw new Error('\n\nFirst letter is not upper case\n')
                } else {
                    // else is not necessary but useable
                };
            }
        }
    },
    body: {
        type: Sequelize.TEXT,
        defaultValue: 'My default text'
    },
    password: {
        type: Sequelize.STRING,
        validate: {
            len: {
                args: [2, 5],
                msg: "The password was not between 2 and 5 chars long"
            }
        }
    }
},
// options object: remove createdAt and updatedAt timestamps, don't pluralise table name
{
//     timestamps: false,
//     freezeTableName: true
    hooks: {
        // see hooks api for documentation on hooks. There are other methods
        beforeValidate: () => {
            console.log("before validate")
        },
        afterValidate: (Record) => {
            Record.password = bcrypt.hashSync(Record.password, 8);
            console.log("after validate")
            
        },
        beforeCreate: ()=> {
            console.log("before create")
            
        },
        afterCreate: (res) => {
            console.log("Created artice with title: ", res.dataValues.title)

        }
    }
}
);

connection.sync({
    // force: true will DROP table if exists, before CREATE table if NOT exists.  
    force: true
}
).then(function () {
    Record.create({
        // persistant instance of the model
        // first arg to create: data, 2nd arg: fields to update
        title: "My Title",
        body: "Body text",
        password: "Matt"
    }, {fields: ['title', 'body', 'password']})
}).catch((error) => {
    console.log(error)
});

// non persistant instance allows you to build it, and won't commit to the database until you call .save()
// nonPersistanrInstance.save();
// non-persistance instances allow you to use contextual methods, useful for relational dbs. 
// both the create function and the save function are asynchronous. Might take a few seconds, immediately return a Promise obj, with a default status of pending.
// you can attach a callback for when the promise has been fulfilled
myNonPersistantInstance = Record.build({
    title: "Ossum new title",
    body: "Body text",
    password: "Matt"
});



let users = [
    {
        name: 'Amanda',
        email: 'myemail@email.com'
    },
    {
        name: 'Bob',
        email: 'bobsemail@email.com'
    },
    {
        name: 'Sandy',
        email: 'sandysemail@email.com'
    },
];

// localhost:3000/
router.get('/user/:id', ctx => {
    ctx.body = users[ctx.params.id];
});

// ctx.request.body is not accessible if you don;t use koa-body
router.post('/user/:id', ctx => {
    ctx.body = Object.assign(users[ctx.params.id], ctx.request.body); 
});

app
    .use(require('koa-body')())
    .use(router.allowedMethods())
    .use(router.routes());

app.listen(3000);

